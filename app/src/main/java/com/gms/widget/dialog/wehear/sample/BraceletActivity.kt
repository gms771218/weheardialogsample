package com.gms.widget.dialog.wehear.sample

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

class BraceletActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bracelet)

        setLyEnable(findViewById<ViewGroup>(R.id.ly_remind_sit))
        setLyEnable(findViewById<ViewGroup>(R.id.ly_remind_drink))
    }

    protected fun setLyEnable(viewGroup: ViewGroup) {
        viewGroup.isEnabled = false
        for (i in 0..viewGroup.childCount) {
            if (viewGroup.getChildAt(i) is TextView)
                viewGroup.getChildAt(i).isEnabled = false
        }
    }


}
