package com.gms.widget.dialog.wehear.sample

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.CheckBox
import android.widget.Toast

/**
 * 01111_新增鬧鐘_鬧鐘重複
 */
class AlarmClockActivity : AppCompatActivity() {

    val CK_IDS = intArrayOf(R.id.cb_week_1, R.id.cb_week_2, R.id.cb_week_3, R.id.cb_week_4, R.id.cb_week_5, R.id.cb_week_6, R.id.cb_week_7)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_alarm_clock)


        findViewById<Button>(R.id.btn_check).setOnClickListener {
            var msg: String = ""
            for (index in CK_IDS) {
                if (findViewById<CheckBox>(index).isChecked) {
                    // ?: 左側為null時，右側才會執行
                    msg.takeIf { !msg.isEmpty() }?.run { msg += "\n" }
                    msg += findViewById<CheckBox>(index).text.toString()
                }
            }
            Toast.makeText(this , "${msg}" , Toast.LENGTH_SHORT).show()
        }

    }


}
