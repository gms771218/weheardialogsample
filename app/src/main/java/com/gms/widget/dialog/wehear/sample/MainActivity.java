package com.gms.widget.dialog.wehear.sample;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.gms.widget.dialog.BaseDialog;
import com.gms.widget.dialog.CallingDialog_2;
import com.gms.widget.dialog.DoorDialog_3;
import com.gms.widget.dialog.FireDialog_1;
import com.gms.widget.dialog.FireDialog_1_ForFamily;
import com.gms.widget.dialog.HeartDialog_1;
import com.gms.widget.dialog.OxygenDialog_1;
import com.gms.widget.dialog.OxygenDialog_1_ForFamily;
import com.gms.widget.dialog.OxygenDialog_2;
import com.gms.widget.dialog.PhoneDialog_3;
import com.gms.widget.dialog.SOSDialog_1;
import com.gms.widget.dialog.SleepingDialog_2;
import com.gms.widget.dialog.WalkingDialog_2;
import com.gms.widget.dialog.WalkingDialog_3;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {


    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.activity_main);

        listView = this.findViewById(R.id.listView);
        listView.setAdapter(new Adapter(this, android.R.layout.simple_list_item_1, createData()));
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                createData().get(position).show(getSupportFragmentManager(), "B");
            }
        });
    }


    ArrayList<BaseDialog> data = new ArrayList<BaseDialog>();

    List<BaseDialog> createData() {
        if (data.size() > 0)
            return data;
        data = new ArrayList<BaseDialog>();
        data.add(new OxygenDialog_1());
        data.add(new OxygenDialog_2());
        data.add(new WalkingDialog_2());
        data.add(new WalkingDialog_3());
        data.add(new SleepingDialog_2());
        data.add(new CallingDialog_2());
        data.add(new PhoneDialog_3());
        data.add(new DoorDialog_3());
        data.add(new HeartDialog_1());
        data.add(new OxygenDialog_1_ForFamily()) ;
        data.add(new FireDialog_1());
        data.add(new FireDialog_1_ForFamily()) ;
        data.add(new SOSDialog_1());
        return data;
    }


    class Adapter extends ArrayAdapter<BaseDialog> {
        public Adapter(@NonNull Context context, int resource, @NonNull List<BaseDialog> objects) {
            super(context, resource, objects);
        }
    }

}
