package com.gms.widget.dialog.wehear.sample.deaf

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.gms.widget.layout.R

/**
 * 步行目標設定
 */
class SettingWalkDeafFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var view = inflater.inflate(R.layout.layout_edit_walk_count, container, false)
        initView(view)
        return view
    }

    open fun initView(view: View) {

    }
}