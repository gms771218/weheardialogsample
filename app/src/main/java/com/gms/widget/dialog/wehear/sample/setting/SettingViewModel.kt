package com.gms.widget.dialog.wehear.sample.setting

import androidx.annotation.IntRange
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel;

class SettingViewModel : ViewModel() {


    // 選項
    var mMenuOption = 0

    var liveData: MutableLiveData<Int> = MutableLiveData<Int>()


    fun getMenuOption(): LiveData<Int> {
        return liveData
    }

    fun setMenuOption(@IntRange(from = 0 , to = 5)option: Int) {
        mMenuOption = option
        liveData.postValue(option)
    }

}
