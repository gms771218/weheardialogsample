package com.gms.widget.dialog.wehear.sample

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.gms.widget.layout.component.AzulLineChart

class MyRecordActivity : AppCompatActivity() {


    var handle = Handler()

    lateinit var lineChartOxygen: AzulLineChart

    lateinit var lineChartHeart: AzulLineChart

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_record)
        lineChartHeart = findViewById<AzulLineChart>(R.id.line_chart_heart)
        lineChartOxygen = findViewById<AzulLineChart>(R.id.line_chart_oxygen)
        update()
    }

    protected fun update() {

        handle.postDelayed({

            lineChartOxygen.showData((Math.random() * 30 + 70).toFloat())
            if (Math.random() < 0.5f)
                lineChartHeart.showData((Math.random() * 150 + 50).toFloat())
            else
                lineChartHeart.showData(listOf<Float>(50f, 50f, 70f, 50f))

            update()

        }, 1000)

    }


}
