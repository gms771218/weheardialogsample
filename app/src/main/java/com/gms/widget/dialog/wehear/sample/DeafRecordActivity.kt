package com.gms.widget.dialog.wehear.sample

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import com.gms.widget.layout.component.WeekTextView

class DeafRecordActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_deaf_record)

        // 設定選取狀態
        findViewById<WeekTextView>(R.id.tv_week_1).isSelected = false
        // 設定樣式
        findViewById<WeekTextView>(R.id.tv_week_1).setStatus(WeekTextView.Status.SMILE)

        findViewById<WeekTextView>(R.id.tv_week_2).isSelected = false
        findViewById<WeekTextView>(R.id.tv_week_2).setStatus(WeekTextView.Status.SAD)

        findViewById<WeekTextView>(R.id.tv_week_3).isSelected = true
        findViewById<WeekTextView>(R.id.tv_week_3).setStatus(WeekTextView.Status.SMILE)

        findViewById<WeekTextView>(R.id.tv_week_4).isSelected = false
        findViewById<WeekTextView>(R.id.tv_week_4).setStatus(WeekTextView.Status.NON)


        var lab = { v: View -> }
        findViewById<WeekTextView>(R.id.tv_week_1).setOnClickListener(lab)
        findViewById<WeekTextView>(R.id.tv_week_1).setOnClickListener({ v: View -> })
        findViewById<WeekTextView>(R.id.tv_week_1).setOnClickListener { v: View -> }

        init()
    }


    fun init(): Unit {
        foo(10)

        var test: Test = Test()
        var value: Int = test.run { this.getValue(1, 2) + 5 }
        println("value = ${value}")
        value = kotlin.run { test.getValue(1, 2) + 10 }
        println("value = ${value}")
        value = test.getValue(1, 2)
        println("value = ${value}")


        var test2: Test = test.also {
            it.getValue(2, 2)
        }
        var deafRecordActivity0: DeafRecordActivity = also {
            this.foo(1)
            it.foo(2)
        }
        deafRecordActivity0 = also {
            it.foo(12)
            test.getValue(1, 3)
        }


        var test3: Test = test.apply {
            getValue(1, 0)
        }
        var deafRecordActivity1: DeafRecordActivity = apply {
            foo(1)
        }
        deafRecordActivity1 = apply {
            this.foo(2)
        }


        // with視最後一行的Function的回傳資料做為回傳值
        value = with(test, {
            this.getValue(1, 3)
        })

        var str: String = with(test, {
            getValue(1, 3)
            toString()
        })


       var value1 : Int = let {
            this.foo(10)
            test.getValue(1,3)
        }

        var value2 : Unit = let {
            test.getValue(1,3)
            this.foo(10)
        }
    }


    fun foo(a: Int): Unit {

    }

    class Test {
        fun getValue(a: Int, b: Int): Int {
            return a + b
        }
    }

}
