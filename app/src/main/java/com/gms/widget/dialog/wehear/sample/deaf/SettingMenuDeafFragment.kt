package com.gms.widget.dialog.wehear.sample.deaf

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModelProviders
import com.gms.widget.dialog.wehear.sample.setting.SettingViewModel
import com.gms.widget.layout.R

/**
 * 04_設定_家人
 */
open class SettingMenuDeafFragment : Fragment() {


    lateinit var btnUser: View
    lateinit var btnWalk: View
    lateinit var btnHeart: View
    lateinit var btnFamily: View
    lateinit var btnNotification: View

    lateinit var imgUserPoint: View
    lateinit var imgWalkPoint: View
    lateinit var imgHeartPoint: View
    lateinit var imgFamilyPoint: View
    lateinit var imgNotificationPoint: View

    private lateinit var viewModel: SettingViewModel


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var view = inflater.inflate(R.layout.layout_setting_menu, container, false)
        initView(view)
        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(activity as FragmentActivity).get(SettingViewModel::class.java)
    }

    open fun initView(view: View) {
        btnUser = view.findViewById(R.id.layout_user)
        btnWalk = view.findViewById(R.id.layout_walk)
        btnHeart = view.findViewById(R.id.layout_heart)
        btnFamily = view.findViewById(R.id.layout_family)
        btnNotification = view.findViewById(R.id.layout_notification)

        imgUserPoint = view.findViewById(R.id.img_user_point)
        imgWalkPoint = view.findViewById(R.id.img_walk_point)
        imgHeartPoint = view.findViewById(R.id.img_heart_point)
        imgFamilyPoint = view.findViewById(R.id.img_family_point)
        imgNotificationPoint = view.findViewById(R.id.img_notification_point)


        btnUser.setOnClickListener {
            viewModel.setMenuOption(1) }
        btnWalk.setOnClickListener { viewModel.setMenuOption(2) }
        btnHeart.setOnClickListener { viewModel.setMenuOption(3) }
        btnFamily.setOnClickListener { viewModel.setMenuOption(4) }
        btnNotification.setOnClickListener { viewModel.setMenuOption(5) }

    }


}