package com.gms.widget.dialog.wehear.sample.common

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.gms.widget.layout.R
import com.gms.widget.layout.R.id.layout_user_height

class UserInfoFg : Fragment() {

    lateinit var imgHeader: ImageView
    lateinit var tvSex: TextView
    lateinit var tvAge: TextView

    lateinit var tvHeight: TextView
    lateinit var tvWeight: TextView
    lateinit var tvStepSize: TextView
    lateinit var tvAddress: TextView

    lateinit var btnLogout: Button

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var view = inflater.inflate(R.layout.layout_user_info, container, false)
        initView(view)
        return view;
    }

    fun initView(view: View) {
        imgHeader = view.findViewById(R.id.img_header)
        btnLogout = view.findViewById(R.id.btn_logout)

        tvSex = view.findViewById(R.id.tv_sex)
        tvAge = view.findViewById(R.id.tv_age)

        layout_user_height
        var layoutHeight = view.findViewById<View>(layout_user_height)
        layoutHeight.findViewById<TextView>(R.id.textView).setText("身高")
        tvHeight = layoutHeight.findViewById(R.id.tv_value)
        tvHeight.setText("178cm")

        var layoutWeight = view.findViewById<View>(R.id.layout_user_weight)
        layoutWeight.findViewById<TextView>(R.id.textView).setText("體重")
        tvWeight = layoutWeight.findViewById(R.id.tv_value)
        tvWeight.setText("72kg")

        var layoutStepSize = view.findViewById<View>(R.id.layout_user_step_size)
        layoutStepSize.findViewById<TextView>(R.id.textView).setText("步長")
        tvStepSize = layoutStepSize.findViewById(R.id.tv_value)
        tvStepSize.setText("98cm")

        var layoutAddress = view.findViewById<View>(R.id.layout_user_address)
        layoutAddress.findViewById<TextView>(R.id.textView).setText("地址")
        tvAddress = layoutAddress.findViewById(R.id.tv_value)
        tvAddress.setText("台北市信義區信義路五段123巷10弄7號10樓-1")


        btnLogout.setOnClickListener {
            Toast.makeText(context , "登出" , Toast.LENGTH_LONG).show()
        }
    }

} // class close