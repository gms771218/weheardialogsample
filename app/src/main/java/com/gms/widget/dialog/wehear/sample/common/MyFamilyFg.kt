package com.gms.widget.dialog.wehear.sample.common

import android.app.AlertDialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.gms.widget.layout.R

/**
 * 我的家人 Sample
 */
class MyFamilyFg : Fragment() {

    lateinit var mRecyclerView: RecyclerView

    var userAdapter = UserAdapter()

    companion object ModelType {
        // 檢視模式
        val MODEL_VIEW_TYPE = View.GONE
        // 編輯模式
        val MODEL_EDIT_TYPE = View.VISIBLE
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var view = inflater.inflate(R.layout.layout_list, container, false)
        initView(view)
        return view
    }

    override fun onResume() {
        super.onResume()
        createData()
    }

    fun initView(view: View) {
        mRecyclerView = view.findViewById(R.id.recyclerView)
        mRecyclerView.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        mRecyclerView.adapter = userAdapter
    }


    fun setModelType(type: Int) {
        userAdapter.setModelType(type)
    }

    /**
     * 建立測試資料
     */
    fun createData() {

        var list = ArrayList<UserVO>()
        for (i in 0..10) {
            list.add(UserVO(i, "NAME : " + i, "091000211" + i))
        }
        list.get(0).type = UserVO.USER_DEAF

        list.get(5).type = UserVO.REQUEST_ADD_IN
        list.get(7).type = UserVO.REQUEST_ADD_IN


        if (list.any { it.type == UserVO.REQUEST_ADD_IN }) {
            list.add(UserVO.createEmptyUser(UserVO.HEADER_TYPE))
        }

        userAdapter.submitList(list.sortedByDescending { it.type })

    }


    class UserAdapter : ListAdapter<UserVO, RecyclerView.ViewHolder>(DiffCallback()) {

        var type = MODEL_EDIT_TYPE

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
            if (viewType == UserVO.USER_FAMILY) {
                var inflater = LayoutInflater.from(parent.context)
                return ViewHolder(inflater.inflate(R.layout.item_family, parent, false))
            } else if (viewType == UserVO.USER_DEAF) {
                var inflater = LayoutInflater.from(parent.context)
                return DeafViewHolder(inflater.inflate(R.layout.item_family_deaf, parent, false))
            } else if (viewType == UserVO.REQUEST_ADD_IN) {
                var inflater = LayoutInflater.from(parent.context)
                return RequestViewHolder(inflater.inflate(R.layout.item_family_add_request, parent, false))
            } else if (viewType == UserVO.HEADER_TYPE) {
                var inflater = LayoutInflater.from(parent.context)
                return RequestHeaderViewHolder(inflater.inflate(R.layout.item_family_add_request_header, parent, false))
            } else {
                var inflater = LayoutInflater.from(parent.context)
                return ViewHolder(inflater.inflate(R.layout.item_family, parent, false))
            }
        }

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
            if (holder is ViewHolder) {
                holder.bind(getItem(position))
                holder.type(type)
            } else if (holder is DeafViewHolder) {
                holder.bind(getItem(position))
                holder.type(type)
            } else if (holder is RequestViewHolder) {
                holder.bind(getItem(position))
            }
        }

        // 類型
        override fun getItemViewType(position: Int): Int {
            return getItem(position).type
        }

        fun setModelType(type: Int) {
            this.type = type
            notifyDataSetChanged()
        }

        class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            fun bind(user: UserVO) {
                itemView.findViewById<TextView>(R.id.tv_name).text = user.name
                itemView.findViewById<TextView>(R.id.tv_phone).text = user.phone
                itemView.findViewById<ImageButton>(R.id.ibtn_delete).setOnClickListener {

                    var builder: AlertDialog.Builder = AlertDialog.Builder(itemView.context)
                    builder.setTitle(itemView.resources.getString(R.string.msg_delete_family_title, "看護", user.name))
                    builder.setMessage(itemView.resources.getString(R.string.msg_delete_family_title))
                    builder.setPositiveButton(itemView.resources.getString(R.string.btn_delete), DialogInterface.OnClickListener { dialog, which ->

                    })
                    builder.setNegativeButton(itemView.resources.getString(R.string.btn_cancel), DialogInterface.OnClickListener { dialog, which ->

                    })

                    var dialog = builder.create()
                    dialog.show()
                    dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(itemView.resources.getColor(R.color.azul_blue_1))
                    dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(itemView.resources.getColor(R.color.azul_blue_1))

                }
            }

            fun type(type: Int) {
                itemView.findViewById<ImageButton>(R.id.ibtn_delete).visibility = type
            }
        }


        class DeafViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            fun bind(user: UserVO) {
                itemView.findViewById<TextView>(R.id.tv_name).text = user.name
                itemView.findViewById<TextView>(R.id.tv_phone).text = user.phone
            }

            fun type(type: Int) {
                itemView.findViewById<ImageButton>(R.id.ibtn_delete).visibility = type
            }
        }

        class RequestViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            fun bind(user: UserVO) {
                itemView.findViewById<TextView>(R.id.tv_name).text = user.name
                itemView.findViewById<TextView>(R.id.tv_phone).text = user.phone
            }
        }

        class RequestHeaderViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    }

    class DiffCallback : DiffUtil.ItemCallback<UserVO>() {
        override fun areItemsTheSame(oldItem: UserVO, newItem: UserVO): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: UserVO, newItem: UserVO): Boolean {
            return oldItem == newItem
        }
    }


    // ==================== User Class ====================

    class UserVO {

        var id: Int = 0
        var name: String
        var phone: String

        // 類型 : 0:家人 ; 1:身障 ; 2 : 新請求資訊
        var type: Int = 0

        companion object {
            fun createEmptyUser(type: Int): UserVO {
                var _user = UserVO(-1, "", "")
                _user.type = type
                return _user
            }

            const val HEADER_TYPE = 0x1000;
            const val USER_FAMILY = 0;
            const val USER_DEAF = 1;
            const val REQUEST_ADD_IN = 2;
        }

        constructor(id: Int, name: String, phone: String) {
            this.id = id
            this.name = name
            this.phone = phone
        }
    }

    // ==================== User Class ====================

} // class close