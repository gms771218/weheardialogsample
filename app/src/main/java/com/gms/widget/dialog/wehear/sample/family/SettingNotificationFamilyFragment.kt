package com.gms.widget.dialog.wehear.sample.family

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.Spinner
import android.widget.Switch
import androidx.fragment.app.Fragment
import com.gms.widget.layout.R

/**
 * 通知提醒 - 家人
 * 045_發布警示_家人
 */
class SettingNotificationFamilyFragment : Fragment() {

    lateinit var switchHeart: Switch
    lateinit var switchOxygen: Switch
    lateinit var switchSleep: Switch
    lateinit var switchWalk: Switch

    lateinit var spinnerSleep: Spinner
    lateinit var spinnerWalk: Spinner

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        var view = inflater.inflate(R.layout.layout_notify_family, container, false)
        initView(view)
        return view
    }

    fun initView(view: View) {
        switchHeart = view.findViewById(R.id.switch_heart)
        switchOxygen = view.findViewById(R.id.switch_oxygen)
        switchSleep = view.findViewById(R.id.switch_sleep)
        switchWalk = view.findViewById(R.id.switch_walk)

        spinnerSleep = view.findViewById(R.id.spinner_sleep_hour)
        spinnerWalk = view.findViewById(R.id.spinner_walk_hour)



        switchHeart.setOnCheckedChangeListener { buttonView, isChecked ->

        }
        switchOxygen.setOnCheckedChangeListener { buttonView, isChecked ->

        }

        switchSleep.setOnCheckedChangeListener { buttonView, isChecked ->

        }

        switchWalk.setOnCheckedChangeListener { buttonView, isChecked ->

        }


        spinnerSleep.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                Log.d("Gms", "value : " + spinnerSleep.adapter.getItem(position))
            }
        }

        spinnerWalk.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                Log.d("Gms", "value : " + spinnerWalk.adapter.getItem(position))
            }
        }

    }


} // class close