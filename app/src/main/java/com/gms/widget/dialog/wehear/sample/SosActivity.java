package com.gms.widget.dialog.wehear.sample;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;

import com.gms.widget.layout.component.SosBar;

public class SosActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sos);

        // ---
        SosBar sosBar = findViewById(R.id.sosbar) ;
        sosBar.setPositionListener(new SosBar.IPositionListener() {

            float posTag =  0 ;

            @Override
            public void onCurrentPos(float position) {

                findViewById(R.id.layout_mask).setAlpha(position);
                if (position == 1) {
                    findViewById(R.id.tv_sos_success).setVisibility(View.VISIBLE);
                }
                if (posTag < position && findViewById(R.id.tv_sos_success).getVisibility() == View.VISIBLE){
                    findViewById(R.id.tv_sos_success).setVisibility(View.INVISIBLE);
                }
                posTag = position ;

            }
        });
    }
}
