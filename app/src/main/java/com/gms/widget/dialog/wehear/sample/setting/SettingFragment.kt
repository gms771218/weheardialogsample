package com.gms.widget.dialog.wehear.sample.setting

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Observer

import com.gms.widget.dialog.wehear.sample.R
import com.gms.widget.dialog.wehear.sample.deaf.SettingHeartDeafFragment
import com.gms.widget.dialog.wehear.sample.deaf.SettingMenuDeafFragment
import com.gms.widget.dialog.wehear.sample.deaf.SettingWalkDeafFragment
import com.gms.widget.dialog.wehear.sample.common.MyFamilyFg
import com.gms.widget.dialog.wehear.sample.common.UserInfoFg
import com.gms.widget.dialog.wehear.sample.deaf.SettingNotificationDeafFragment
import com.gms.widget.dialog.wehear.sample.family.SettingNotificationFamilyFragment

/**
 * 設定選單
 */
class SettingFragment : Fragment() {

    companion object {
        fun newInstance() = SettingFragment()
    }

    private lateinit var viewModel: SettingViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.setting_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(activity as FragmentActivity).get(SettingViewModel::class.java)
        viewModel.setMenuOption(0)
        viewModel.getMenuOption().observe(this, Observer { type: Int ->

            when (type) {

                0 -> transPage(SettingMenuDeafFragment())
                1 -> transPage(UserInfoFg())
                2 -> transPage(SettingWalkDeafFragment())
                3 -> transPage(SettingHeartDeafFragment())
                4 -> transPage(MyFamilyFg())
                5 -> transPage(SettingNotificationDeafFragment())
            }

        })
    }

    override fun onStart() {
        super.onStart()

    }


    fun transPage(fragment: Fragment) {
        var fragmentTransaction = childFragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.layout_container, fragment)
        fragmentTransaction.addToBackStack(null).commit()
    }



}
